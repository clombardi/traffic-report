package ar.org.clombardi.fileCrunching.process;

import java.io.PrintWriter;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import ar.org.clombardi.fileCrunching.exceptions.DataParsingException;
import ar.org.clombardi.fileCrunching.format.TrafficReportCsvFormatter;
import ar.org.clombardi.fileCrunching.format.TrafficReportInfoFormatter;
import ar.org.clombardi.fileCrunching.model.RecordedRequest;
import ar.org.clombardi.fileCrunching.model.RequestEntity;

/**
 * My task is to write out in my output the traffic report generated from
 * the records given by my input. 
 * 
 * I traverse the input and give each request description to a RequestAccumulator.
 * Then I take the result of the accumulation, and write a representation of each
 * element that represents an entity into my output.
 * 
 * I delegate to a <code>TrafficReportInfoFormatter</code> the task of generating
 * the string to be appended to the output for each entity.   
 */
public class TrafficReportGenerator {
	private Scanner input;
	private PrintWriter output;
	private TrafficReportInfoFormatter formatter;
	private RequestAccumulator accum = new RequestAccumulator(); 
	private TrafficReportProcessControl processControl = new TrafficReportProcessControl();
	
	public void generateReport() {
		this.accum.reset();
		if (this.formatter == null) {
			this.formatter = new TrafficReportCsvFormatter(); 
		}
		
		// process of each line in the input, halt if dictated by process control
		while (input.hasNextLine() && !processControl.mustHalt()) {
			try {
				RecordedRequest request = new RecordedRequest(input.nextLine());
				boolean accumResult = accum.processRecord(request);
				if (accumResult) { processControl.okRequest(request); } else { processControl.discardedRequest(request); }
			} catch (DataParsingException e) {
				processControl.failedRequest(e);
			}
		}
		
		// if process was interrupted by process control, then no output should be produced
		if (processControl.mustHalt()) { return; }
		
		// obtain a list with the entities involved in the analyzed requests, sorted by number of requests DESC
		List<RequestEntity> sortedEntities = accum.getEntities().stream()
			.sorted(Comparator.comparing(RequestEntity::getNumberOfRequests).reversed())
			.collect(Collectors.toList());

		// writes the sorted results onto the output
		
		// prefix
		if (this.formatter.prefix() != null) {
			output.println(this.formatter.prefix());
		}

		// contents
		// 
		// My first impulse here was to write something nice like
		//		sortedEntities.forEach(entity ->  output.println(this.formatter.format(entity)));
		// but I realized that the separator should be inserted after all lines except the last one.
		// I could have used String.join(...formatted lines..., separator + line.separator) but this approach
		// would imply to have all the output contents in memory, and the output file could be quite large.
		// So I preferred a more traditional, maybe less cool algorithm, but saving space. I realize that
		// space is being traded with time (since each entity is searched by get(i)), but I still think
		// is a good solution.
		String separator = this.formatter.separator();
		if (separator == null) { separator = ""; }
		for (int i = 0; i < sortedEntities.size(); i++) {
			output.println(this.formatter.format(sortedEntities.get(i)) + (i == sortedEntities.size() - 1 ? "" : separator));
		}
		
		// suffix
		if (this.formatter.suffix() != null) {
			output.println(this.formatter.suffix());
		}
	}

	public TrafficReportProcessControl getProcessControl() {
		return processControl;
	}

	public void setProcessControl(TrafficReportProcessControl processControl) {
		this.processControl = processControl;
	}

	public void setInput(Scanner input) {
		this.input = input;
	}

	public void setOutput(PrintWriter output) {
		this.output = output;
	}

	public void setFormatter(TrafficReportInfoFormatter formatter) {
		this.formatter = formatter;
	}
}
