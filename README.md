# Traffic Report Generation

This project implement a process that takes an input file that describes a series of requests, and generates an output file with the total number of requests and bytes for every entity that participated in those requests. An entity is identified by an IP adress.

It takes `testData\ipaddr.csv` and `logfiles\requests.log` as input and output files. In the current versions these paths are fixed, but they could easily be parametrized as command line arguments.

We implemented the basic version that generates a CSV output, and two enhancements: the possibility of generating a JSON instead of CSV output, and the ability to tolerate a quantity of input lines with error, e.g. if we set the number of tolerated error lines to 4, the process will halt at the fifth line with error it encounters, rather than the first one which is the default behavior.  
Both enhancements can be activated by command line parameters when executing the entry point `TrafficReportExecuter`, `--json` and `--tolerance n` respectively.


# Implementation notes
The code is organized in three major parts: process, format and model. We describe each in turn.

## Process
Includes four classes: `TrafficReportExecuter`, `TrafficReportGenerator`, `RequestAccumulator`, and `TrafficReportProcessControl`.

`TrafficReportExecuter` is the entry point, it defines the static `main` method that launch the process. It handles the input and output files, delegating the actual generation to the `TrafficReportGenerator`.

The `TrafficReportGenerator` manages the traversal of the input line, delegating the actual analysis to the `RequestAccumulator`, and then records the information about each entity in the output file. 
The separation between `TrafficReportExecuter` and `TrafficReportGenerator` aims to let the former focus on the file and parameter handling, and also to ease the automatic testing of the latter by providing input and output devices which do not interact with a file system or DB. For that reason, in the `TrafficReportGenerator` point of view, the input is just a `Scanner` and the output a `PrintWriter`.

The `RequestAccumulator` performs the summarizaton of the information of each entity, based on the analysis of the input records provided by the `TrafficReportGenerator`.

Finally, the `TrafficReportProcessControl` allows the process to tolerate a certain amount of errors. This component is informed of the result (valid and status OK, valid and status not OK, or invalid) in the analysis of each input line, and is responsible to indicate whether the process should halt.  
In the current form, it accepts a "tolerance index" in terms of number of admitted invalid lines; the process will halt only when the number of invalid lines processed is bigger thn the given index.
It also provides basic statistics about the process, which are shown in console when it finishes.
If other tolerance criteria are found convenient, a strategy to decide whether the process should halt can be separated from the logic of the `TrafficReportProcessControl` so that different strategies can be made available, without affecting other parts of the program.


## Format
It consists of the `TrafficReportInfoFormatter`, which is responsible to compute the string to be inserted in the output given the information gathered about a request entity, and also to provide global initial and ending strings, and a separator between entities.  
`TrafficReportInfoFormatter` is defined as an abstract class that provides defaults for start/end/separator strings, and also a formatter for numbers that can be handy for the concrete subclasses. The project includes two concrete subclasses, that generate CSV and JSON outputs. Of course, additional formatters can be easily added.

The concrete formatter for an execution must be set to the `TrafficReportExecuter`, the default is a CSV formatter. 


## Model
It includes two classes, `RecordedRequest` and `RequestEntity`. 

`RecordedRequest` represents a request described in the input. This class includes a constructor that gets the input line string as parameter, and does the parsing, throwing an exception for invalid lines.

`RequestEntity` represents the information about an entity that made at least one request. It includes all the information that is included for an entity in the output, including the percentages. 
Hence each `RequestEntity` holds a reference to the `RequestAccumulator`, which uses to obtain the total values for quantity of requests and bytes sent needed to compute the percentages. 
Maybe this is not a very elegant decision, but it came very handy for simplifying the code.


