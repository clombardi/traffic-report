package ar.org.clombardi.fileCrunching.process;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import ar.org.clombardi.fileCrunching.model.RecordedRequest;
import ar.org.clombardi.fileCrunching.model.RequestEntity;

class RequestAccumulatorTest {

	Optional<RequestEntity> getPossibleEntityForIp(String ip, RequestAccumulator accum) {
		return accum.getEntities().stream().filter(ent -> ent.getIpAddress().equals(ip)).findFirst();
	}

	RequestEntity getEntityForIp(String ip, RequestAccumulator accum) {
		return this.getPossibleEntityForIp(ip, accum).get();
	}

	@Test
	void justCreated() {
		RequestAccumulator accum = new RequestAccumulator();
		assertEquals(0, accum.getEntities().size());
		assertEquals(0, accum.getRequestCount());
		assertEquals(0, accum.getTotalBytes());
	}
	
	@Test
	void fourRequests() {
		RequestAccumulator accum = new RequestAccumulator();
		accum.processRecord(new RecordedRequest("10:05,4500,OK,ip_1"));
		accum.processRecord(new RecordedRequest("10:06,2000,OK,ip_2"));
		accum.processRecord(new RecordedRequest("10:07,4500,ERR,ip_1"));
		accum.processRecord(new RecordedRequest("10:08,3500,OK,ip_2"));
		assertEquals(2, accum.getEntities().size());
		assertEquals(3, accum.getRequestCount());
		assertEquals(10000, accum.getTotalBytes());
		RequestEntity ip1 = this.getEntityForIp("ip_1", accum); 
		assertEquals(4500, ip1.getBytesSent());
		assertEquals(1, ip1.getNumberOfRequests());
		assertEquals(45, ip1.getPercentageOfBytes(), 0.01);
		assertEquals(33.33, ip1.getPercentageOfRequests(), 0.01);
		RequestEntity ip2 = this.getEntityForIp("ip_2", accum); 
		assertEquals(5500, ip2.getBytesSent());
		assertEquals(2, ip2.getNumberOfRequests());
		assertEquals(55, ip2.getPercentageOfBytes(), 0.01);
		assertEquals(66.67, ip2.getPercentageOfRequests(), 0.01);
		assertFalse(this.getPossibleEntityForIp("ip_3", accum).isPresent());
	}
	
	@Test
	void manyRequests() {
		RequestAccumulator accum = new RequestAccumulator();
		IntStream.rangeClosed(1, 10).forEach(n -> accum.processRecord(
				new RecordedRequest(String.format("10:%s,%s,OK,ip_1", n+10, n*1000)))); // 55000
		Stream.of("10:40,3000,OK,ip_2", "10:41,8000,OK,ip_3","10:42,7000,OK,ip_2", "10:43,4500,ERR,ip_4",
				"10:44,4000,OK,ip_3","10:45,5000,ERR,ip_3","10:46,6000,OK,ip_5","10:47,5000,OK,ip_2",
				"11:44,5000,OK,ip_1","10:45,1000,OK,ip_3","10:46,4000,OK,ip_5","10:47,2000,OK,ip_2")
				.forEach(str -> accum.processRecord(new RecordedRequest(str))); // 45000
		assertEquals(4, accum.getEntities().size());
		assertEquals(20, accum.getRequestCount());
		assertEquals(100000, accum.getTotalBytes());
		RequestEntity ip1 = this.getEntityForIp("ip_1", accum); 
		assertEquals(60000, ip1.getBytesSent());
		assertEquals(11, ip1.getNumberOfRequests());
		assertEquals(60, ip1.getPercentageOfBytes(), 0.01);
		assertEquals(55, ip1.getPercentageOfRequests(), 0.01);
		RequestEntity ip2 = this.getEntityForIp("ip_2", accum); 
		assertEquals(17000, ip2.getBytesSent());
		assertEquals(4, ip2.getNumberOfRequests());
		assertEquals(17, ip2.getPercentageOfBytes(), 0.01);
		assertEquals(20, ip2.getPercentageOfRequests(), 0.01);
		assertFalse(this.getPossibleEntityForIp("ip_4", accum).isPresent());		
	}
}
