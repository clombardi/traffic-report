package ar.org.clombardi.fileCrunching.process;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

import ar.org.clombardi.fileCrunching.format.TrafficReportJsonFormatter;

/**
 * I execute the task of actually generating a traffic report.
 * I handle input and output files, and give the necessary objects to a
 * <code>TrafficReportGenerator</code> to let it fill the output 
 * with the expected report.
 * 
 * Accepted parameters at the command line
 * --json yields the report in JSON, rather than CSV, format.
 */
public class TrafficReportExecuter {
	private String inputFileName = "./testData/ipaddr.csv";
	private String outputFileName = "./logfiles/requests.log";
	
	private boolean jsonFormat = false;
	private int toleranceIndex = 0;

	public static void main(String[] args) {
		TrafficReportExecuter executer = new TrafficReportExecuter();
		executer.analyzeArguments(args);
		executer.generateReport();
	}
	
	public void analyzeArguments(String[] args) {
		int ix = 0;
		while (ix < args.length) {
			if (args[ix].toLowerCase().equals("--json")) {
				this.jsonFormat = true;
				ix++;
			} else if (args[ix].toLowerCase().equals("--tolerance")) {
				ix++;
				if (ix < args.length) {
					try {
						this.toleranceIndex = Integer.parseInt(args[ix]);
					} catch (NumberFormatException e) {
						// just ignore --tolerance and go on
					}
					ix++;
				}
			} else {
				// unknown argument - just ignore
				ix++;
			}
			
		}
	}
	public void generateReport() {
		Scanner input = null;
		PrintWriter output = null;
		try {
			input = new Scanner(new File(this.inputFileName));
			output = new PrintWriter(new FileWriter(this.outputFileName));
			TrafficReportGenerator generator = new TrafficReportGenerator();
			if (this.jsonFormat) {
				generator.setFormatter(new TrafficReportJsonFormatter());
			}
			if (this.toleranceIndex > 0) {
				TrafficReportProcessControl processControl = new TrafficReportProcessControl();
				processControl.setToleratedErrors(this.toleranceIndex);
				generator.setProcessControl(processControl);
			}
			
			generator.setInput(input);
			generator.setOutput(output);
			generator.generateReport();
			System.out.println(generator.getProcessControl().getProcessReport());
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			if (input != null) {
				input.close();
			}
			if (output != null) {
				output.close();
			}
		}
		
	}
}
