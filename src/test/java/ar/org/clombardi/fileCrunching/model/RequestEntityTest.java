package ar.org.clombardi.fileCrunching.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

class RequestEntityTest {

	@Test
	public void emptyRequestEntity() {
		RequestEntity re = new RequestEntity(null, "some.ip.address"); 
		assertEquals(0, re.getBytesSent());
		assertEquals(0, re.getNumberOfRequests());
	}
	
	@Test
	public void twoRequests() {
		RequestEntity re = new RequestEntity(null, "some.ip.address");
		re.recordRequest(100);
		re.recordRequest(3000);
		assertEquals(3100, re.getBytesSent());
		assertEquals(2, re.getNumberOfRequests());
	}

	@Test
	public void manyRequests() {
		RequestEntity re = new RequestEntity(null, "some.ip.address");
		IntStream.rangeClosed(1, 10).map(n -> n * 100).forEach(n -> re.recordRequest(n));
		re.recordRequest(3200);
		assertEquals(8700, re.getBytesSent());
		assertEquals(11, re.getNumberOfRequests());
	}

}
