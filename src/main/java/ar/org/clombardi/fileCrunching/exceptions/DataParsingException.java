package ar.org.clombardi.fileCrunching.exceptions;

/**
 * An exception related with the processing of the input data.
 */
public class DataParsingException extends RuntimeException {
	private static final long serialVersionUID = -6556491519109553004L;

	public DataParsingException() {
		super();
	}

	public DataParsingException(String message) {
		super(message);
	}
}
