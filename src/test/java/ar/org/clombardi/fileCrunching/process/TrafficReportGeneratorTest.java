package ar.org.clombardi.fileCrunching.process;

import static org.junit.jupiter.api.Assertions.*;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Scanner;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import ar.org.clombardi.fileCrunching.format.TrafficReportJsonFormatter;

class TrafficReportGeneratorTest {

	String sep = System.getProperty("line.separator");
	
	// input: Scanner over String
	Scanner buildInput() {
		String linesWithEnter = Stream.of(
				"10:40,3000,OK,ip_2", "10:41,500,OK,ip_3", "10:42,5500,OK,ip_2", "10:43,1200,OK,ip_4", "10:44,800,OK,ip_4", "10:45,1500,OK,ip_2"
				).reduce("", (result, elem) -> result.equals("") ? elem : result + "\n" + elem);
		Scanner input = new Scanner(linesWithEnter);
		input.useDelimiter("\n");
		return input;
	}
	
	// just to learn how to create Scanner and PrintWriter on memory
	@Test
	void scannerWriterBehavior() {
		Scanner input = this.buildInput();

		// output: PrintWriter to String
		StringWriter outputStore = new StringWriter();
		PrintWriter output = new PrintWriter(outputStore);

		// tests
		assertEquals("10:40,3000,OK,ip_2", input.nextLine());
		assertEquals("10:41,500,OK,ip_3", input.nextLine());
		output.println("linea1");
		output.println("linea2");
		assertEquals("linea1", outputStore.toString().substring(0, 6));
		assertEquals(String.format("linea1%slinea2%s", sep, sep), outputStore.toString());

		// end action: close input and output
		input.close();
		output.close();
	}

	
	@Test
	void csvOutput() {
		Scanner input = this.buildInput();

		// output: PrintWriter to String
		StringWriter outputStore = new StringWriter();
		PrintWriter output = new PrintWriter(outputStore);
		

		// launch TrafficReportGenerator
		TrafficReportGenerator generator = new TrafficReportGenerator();
		generator.setInput(input);
		generator.setOutput(output);
		generator.generateReport();
		
		// check the output
		assertEquals(
				"ip_2,3,50.00,10000,80.00#ip_4,2,33.33,2000,16.00#ip_3,1,16.67,500,4.00#".replace("#", sep), 
				outputStore.toString());
		
		// end action: close input and output
		input.close();
		output.close();
	}

	
	@Test
	void jsonOutput() {
		Scanner input = this.buildInput();

		// output: PrintWriter to String
		StringWriter outputStore = new StringWriter();
		PrintWriter output = new PrintWriter(outputStore);
		

		// launch TrafficReportGenerator
		TrafficReportGenerator generator = new TrafficReportGenerator();
		generator.setInput(input);
		generator.setOutput(output);
		generator.setFormatter(new TrafficReportJsonFormatter());
		generator.generateReport();
		
		// check the output
		assertEquals(("[#" +
				"{\"ipAddress\":\"ip_2\",\"requests\":3,\"requestPercentage\":50.00,\"bytes\":10000,\"bytesPercentage\":80.00},#" + 
				"{\"ipAddress\":\"ip_4\",\"requests\":2,\"requestPercentage\":33.33,\"bytes\":2000,\"bytesPercentage\":16.00},#" + 
				"{\"ipAddress\":\"ip_3\",\"requests\":1,\"requestPercentage\":16.67,\"bytes\":500,\"bytesPercentage\":4.00}#" +
				"]#").replace("#", sep),
				outputStore.toString());
		
		// end action: close input and output
		input.close();
		output.close();
	}
}
