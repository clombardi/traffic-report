package ar.org.clombardi.fileCrunching.format;

import ar.org.clombardi.fileCrunching.model.RequestEntity;

public class TrafficReportCsvFormatter extends TrafficReportInfoFormatter {
	public TrafficReportCsvFormatter() {
		super();
	}

	public String format(RequestEntity entity) {
		return entity.getIpAddress() + ","
				+ entity.getNumberOfRequests() + ","
				+ this.numberFormat.format(entity.getPercentageOfRequests()) + ","
				+ entity.getBytesSent() + ","
				+ this.numberFormat.format(entity.getPercentageOfBytes());
	}

}
