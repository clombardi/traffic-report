package ar.org.clombardi.fileCrunching.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ar.org.clombardi.fileCrunching.exceptions.DataParsingException;

class RecordedRequestTest {

	@Test
	public void testHappyCase() {
		RecordedRequest request = new RecordedRequest("10:18,450,OK,10.4.10.3");
		assertEquals(450, request.getBytesSent());
		assertEquals("10.4.10.3", request.getClientIpAddress());
		assertEquals("OK", request.getStatus());
		assertEquals("10:18", request.getTimestamp());
	}

	@Test
	public void testTooFewFields() {
		String line = "10:18,450,OK";
		Exception exception = assertThrows(DataParsingException.class, () -> {
			new RecordedRequest(line);
		});
		assertEquals(String.format("Source line %s: found 3 items of data instead of 4", line), exception.getMessage());
	}

	@Test
	public void testTooManyFields() {
		String line = "10:18,450,OK,10.4.10.3,hello";
		Exception exception = assertThrows(DataParsingException.class, () -> {
			new RecordedRequest(line);
		});
		assertEquals(String.format("Source line %s: found 5 items of data instead of 4", line), exception.getMessage());
	}

	@Test
	public void testBytesIsNotANumber() {
		String line = "10:18,not_a_number,OK,10.4.10.3";
		Exception exception = assertThrows(DataParsingException.class, () -> {
			new RecordedRequest(line);
		});
		assertEquals("Value not_a_number of bytesSent is not a valid number representation", exception.getMessage());
	}
}
