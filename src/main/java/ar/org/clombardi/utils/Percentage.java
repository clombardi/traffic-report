package ar.org.clombardi.utils;

public class Percentage {
	private double part;
	private double total;

	public Percentage(double part, double total) {
		super();
		this.part = part;
		this.total = total;
	}

	public Percentage(double total) {
		super();
		this.total = total;
	}

	public double getValue() {
		return (this.part * 100) / this.total;
	}

	public double getValue(double part) {
		return (part * 100) / this.total;
	}
}
