package ar.org.clombardi.fileCrunching.model;

import ar.org.clombardi.fileCrunching.exceptions.DataParsingException;

/**
 * I am an object representation of an input line that described a single request.
 */
public class RecordedRequest {
	private String timestamp;
	private long bytesSent;
	private String status;
	private String clientIpAddress;
	
	public RecordedRequest(String record) {
		String[] data = record.split(",");
		if (data.length != 4) {
			throw new DataParsingException(
					String.format("Source line %s: found %s items of data instead of 4", record, data.length));
		}
		this.timestamp = data[0];
		try { 
			this.bytesSent = Long.parseLong(data[1]); 
		} catch (NumberFormatException e) {
			throw new DataParsingException(
					String.format("Value %s of bytesSent is not a valid number representation", data[1]));
		}
		this.status = data[2];
		this.clientIpAddress = data[3];
	}

	public String getTimestamp() {
		return timestamp;
	}

	public long getBytesSent() {
		return bytesSent;
	}

	public String getStatus() {
		return status;
	}

	public String getClientIpAddress() {
		return clientIpAddress;
	}
}
