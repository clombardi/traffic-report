package ar.org.clombardi.fileCrunching.process;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import ar.org.clombardi.fileCrunching.model.RecordedRequest;
import ar.org.clombardi.fileCrunching.model.RequestEntity;

/**
 * I know how to summarize the information included in a series of request descriptions.
 * I am not responsible for traversing the input stream, just to process each request description
 * in turn, when some other object that performs the traversing decides to do so.
 * 
 * I discard the requests whose status is not "OK".
 */
public class RequestAccumulator {
	private Collection<RequestEntity> entities = new ArrayList<RequestEntity>();
	private long requestCount = 0;
	private long totalBytes = 0;
	
	/**
	 * Process a request included in the set of input data
	 */
	public boolean processRecord(RecordedRequest request) {
		// discard non-OK requests
		if (!"OK".equals(request.getStatus())) { return false; }
		
		// updating of own totals
		this.requestCount += 1;
		this.totalBytes += request.getBytesSent();
		
		// updating, and eventually creation, of the entity related to the request
		Optional<RequestEntity> foundEntity = this.entities.stream()
				.filter(entity -> entity.getIpAddress().contentEquals(request.getClientIpAddress()))
				.findFirst();
		RequestEntity requestEntityForThisRequest;
		if (foundEntity.isPresent()) {
			requestEntityForThisRequest = foundEntity.get();
		} else {
			requestEntityForThisRequest = new RequestEntity(this, request.getClientIpAddress());
			entities.add(requestEntityForThisRequest);
		}
		requestEntityForThisRequest.recordRequest(request.getBytesSent());
		return true;
	}

	public Collection<RequestEntity> getEntities() {
		return entities;
	}

	public long getRequestCount() {
		return requestCount;
	}

	public long getTotalBytes() {
		return totalBytes;
	}
	
	public void reset() {
		this.entities = new ArrayList<RequestEntity>();
		this.requestCount = 0;
		this.totalBytes = 0;
	}
}
