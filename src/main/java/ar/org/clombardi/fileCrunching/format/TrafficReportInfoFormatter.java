package ar.org.clombardi.fileCrunching.format;

import java.text.NumberFormat;

import ar.org.clombardi.fileCrunching.model.RequestEntity;

/**
 * I represent a policy of how the information included in a traffic report
 * is represented as Strings.
 * 
 */
public abstract class TrafficReportInfoFormatter {
	protected NumberFormat numberFormat;

	public TrafficReportInfoFormatter() {
		this.numberFormat = NumberFormat.getNumberInstance();
		this.numberFormat.setMaximumFractionDigits(2);
		this.numberFormat.setMinimumFractionDigits(2);
	}

	public abstract String format(RequestEntity entity);
	public String prefix() { return null; }
	public String suffix() { return null; }
	public String separator() { return null; }
}
