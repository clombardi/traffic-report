package ar.org.clombardi.fileCrunching.format;

import ar.org.clombardi.fileCrunching.model.RequestEntity;

public class TrafficReportJsonFormatter extends TrafficReportInfoFormatter {
	public TrafficReportJsonFormatter() {
		super();
	}

	public String format(RequestEntity entity) {
		// it would have been nicer to use some library that handles JSON processing.
		// I preferred to avoid using libraries for this small project.
		// If more JSON processing is needed, then some library should be incorporated
		// into the project.
		return "{" 
				+ "\"ipAddress\":\"" + entity.getIpAddress() + "\","
				+ "\"requests\":" + entity.getNumberOfRequests() + ","
				+ "\"requestPercentage\":" + this.numberFormat.format(entity.getPercentageOfRequests()) + ","
				+ "\"bytes\":" + entity.getBytesSent() + ","
				+ "\"bytesPercentage\":" + this.numberFormat.format(entity.getPercentageOfBytes())
				+ "}";
	}

	@Override
	public String prefix() {
		return "[";
	}

	@Override
	public String suffix() {
		return "]";
	}

	public String separator() { 
		return ","; 
	}
	
}
