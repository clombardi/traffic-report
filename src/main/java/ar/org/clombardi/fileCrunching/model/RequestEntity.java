package ar.org.clombardi.fileCrunching.model;

import ar.org.clombardi.fileCrunching.process.RequestAccumulator;
import ar.org.clombardi.utils.Percentage;

/**
 * I represent a specific entity that performed requests, identified by its IP address.
 */
public class RequestEntity {
	private RequestAccumulator accumulator;
	private String ipAddress;
	private long numberOfRequests;
	private long bytesSent;

	
	public RequestEntity(RequestAccumulator accum, String ipAddress) {
		super();
		this.accumulator = accum;
		this.ipAddress = ipAddress;
	}

	public String getIpAddress() {
		return ipAddress;
	}
	
	public long getNumberOfRequests() {
		return numberOfRequests;
	}
	
	public long getBytesSent() {
		return bytesSent;
	}
	
	public double getPercentageOfRequests() {
		return new Percentage(this.numberOfRequests, this.accumulator.getRequestCount()).getValue();
	}

	public double getPercentageOfBytes() {
		return new Percentage(this.bytesSent, this.accumulator.getTotalBytes()).getValue();
	}

	public void recordRequest(long bytesSentInTheRequest) {
		this.numberOfRequests++;
		this.bytesSent += bytesSentInTheRequest;
	}
}
