package ar.org.clombardi.fileCrunching.format;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import ar.org.clombardi.fileCrunching.model.RecordedRequest;
import ar.org.clombardi.fileCrunching.model.RequestEntity;
import ar.org.clombardi.fileCrunching.process.RequestAccumulator;

class TrafficReportInfoFormatterTest {

	RequestEntity buildExampleEntity() {
		RequestAccumulator accum = new RequestAccumulator();
		accum.processRecord(new RecordedRequest("10:40,3000,OK,ip_2"));
		accum.processRecord(new RecordedRequest("10:40,1000,OK,ip_3"));
		return accum.getEntities().stream().filter(entity -> entity.getIpAddress().contentEquals("ip_2")).findFirst().get();
	}
	
	
	@Test
	void csvFormatter() {
		assertEquals("ip_2,1,50.00,3000,75.00", new TrafficReportCsvFormatter().format(this.buildExampleEntity()));
	}

	@Test
	void jsonFormatter() {
		assertEquals(
				"{\"ipAddress\":\"ip_2\",\"requests\":1,\"requestPercentage\":50.00,\"bytes\":3000,\"bytesPercentage\":75.00}", 
				new TrafficReportJsonFormatter().format(this.buildExampleEntity())
		);
	}

}
