package ar.org.clombardi.fileCrunching.process;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ar.org.clombardi.fileCrunching.model.RecordedRequest;


/**
 * I keep control of the evolution of a traffic report process.
 * I decide whether it should halt given the quantity and/or frequency of errors.
 */
public class TrafficReportProcessControl {
	private class ErrorRecord {
		private String message;
		private int lineNumber;
		
		public ErrorRecord(String message, int lineNumber) {
			this.message = message;
			this.lineNumber = lineNumber;
		}
		
		public String getMessageToRender() {
			return String.format("Line %s - message %s", this.lineNumber, this.message);
		}
	}
	
	int requestsProcessed = 0;
	int requestsDiscarded = 0;
	List<ErrorRecord> errorRecords = new ArrayList<ErrorRecord>();
	private int toleratedErrors = 0;
	
	public void setToleratedErrors(int n) {
		this.toleratedErrors = n;
	}

	public void okRequest(RecordedRequest request) {
		this.requestsProcessed++;
	}
	
	public void discardedRequest(RecordedRequest request) {
		this.requestsDiscarded++;
	}
	
	public void failedRequest(Exception errorObtained) {
		this.errorRecords.add(new ErrorRecord(errorObtained.getMessage(), this.totalAnalyzedRequests() + 1));
	}
	
	public int totalAnalyzedRequests() { 
		return this.requestsProcessed + this.requestsDiscarded + this.errorRecords.size();
	}
	
	public boolean mustHalt() {
		return this.errorRecords.size() > this.toleratedErrors;
	}
	
	public String getProcessReport() {
		String summary = String.format(
				"Process %s - %s records analyzed, %s ok, %s discarded, %s with error", 
				this.mustHalt() ? "aborted" : "finished", 
				this.totalAnalyzedRequests(),
				this.requestsProcessed, this.requestsDiscarded, this.errorRecords.size());
		List<String> linesOfReport = this.errorRecords.stream().map(ErrorRecord::getMessageToRender).collect(Collectors.toList());
		linesOfReport.add(0, summary);
		return String.join("\r\n", linesOfReport);
	}
}
